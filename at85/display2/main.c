#ifndef F_CPU
#define F_CPU 8000000L
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#include "tm1637.h"

#include <avr/interrupt.h>

volatile uint8_t digit = 0;

ISR (INT0_vect) {
  led_print(0, "tE5t");

}

int main(void) {
  sei();
  GIMSK |= (1 << INT0);
  MCUCR |= (1 << ISC01);

  _delay_ms(1000);

  TM1637_SERIAL_INIT;

  led_dots(0x01);
  led_print(0, "0012");

  while (1) {

  }

}
