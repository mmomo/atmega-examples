#ifndef F_CPU
#define F_CPU 8000000L
#endif

#include <avr/io.h>
#include <util/delay.h>

int main(void) {
  // pin b3 as output, b0 as input
  DDRB = 0x08;

  while (1) {
    if (PINB & 0x01) { // if pb0 high
      PORTB = 0x08;
    } else {
      PORTB = 0x00;
    }    
  }

  return 1;
}
