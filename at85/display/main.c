#ifndef F_CPU
#define F_CPU 8000000L
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#include "tm1637.c"

#include <avr/interrupt.h>

volatile uint8_t digit = 0;

ISR (INT0_vect) {
  digit++;
  _delay_ms(100);
}

int main(void) {
  sei();
  GIMSK |= (1 << INT0);
  MCUCR |= (1 << ISC01);

  _delay_ms(1000);

  TM1637_init(1, 0);

  TM1637_clear();

  TM1637_display_digit(0x00, 0);
  TM1637_display_digit(0x01, 0);
  TM1637_display_digit(0x02, 0);
  TM1637_display_digit(0x03, 0);

  TM1637_display_colon(1);

  while (1) {
    TM1637_display_digit(0x03, digit % 0x0A);
    _delay_ms(100);
  }

}
