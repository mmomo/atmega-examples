#ifndef F_CPU
#define F_CPU 8000000L
#endif

#define __DELAY_BACKWARD_COMPATIBLE__

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "tm1637.c"

#define setBit(bit) PORTB |= (1 << bit)
#define clearBit(bit) PORTB &= ~(1 << bit)

#define BUZZER PB4

typedef enum State {
  WORK,
  REST,
  LONG_REST
} State;

const int WORK_TIME = 25 * 60;
const int REST_TIME = 5 * 60;
const int LONG_REST_TIME = 30 * 60;
const int CYCLES = 4;

volatile int seconds = WORK_TIME;
volatile int should_pause = 0;
State current_state = WORK;


/* FUNCTION PROTOTYPES */
void setup_display(void);
void write_digits(void);
void blink_digits(void);
void play_buzzer(int, long);
void change_state(State);


/* MAIN CODE */
int main(void) {

  DDRB = 0x13;
  PORTB |= (1 << PB2) | (1 << PB3);

  sei();
  GIMSK |= (1 << INT0);
  MCUCR |= (1 << ISC01);

  setup_display();

  int current_iteration = 0;

  while (1) {

    if (!should_pause) {

      if (seconds >= 0) {
        write_digits();

        seconds--;
        _delay_ms(1000);

      } else {

        play_buzzer(2, 200);

        should_pause = 1;


        switch (current_state) {
        case WORK:
          change_state(REST);
          write_digits();
          break;

        case REST:
          if (current_iteration == CYCLES - 1) {
            change_state(LONG_REST);
          } else {
            change_state(WORK);
          }

          write_digits();
          ++current_iteration;
          break;

        case LONG_REST:
          seconds = 0;
          current_iteration = 0;
          play_buzzer(10, 100);
          break;
        }
      }
    } else {
      blink_digits();
    }

  }

  return 0;
}

/* INTERRUPTIONS */

ISR (INT0_vect) {
  // toggle state

  should_pause = !should_pause;
}


/* FUNCTIONS */

void setup_display() {
  TM1637_init(1, 0);

  TM1637_clear();

  TM1637_display_digit(0x00, 0);
  TM1637_display_digit(0x01, 0);
  TM1637_display_digit(0x02, 0);
  TM1637_display_digit(0x03, 0);

  TM1637_display_colon(1);
}

void write_digits() {
    int total_seconds = seconds % 60;

    int seconds_units = total_seconds % 10;
    int seconds_tens = total_seconds / 10;

    int total_mins = seconds / 60;

    int minutes_units = total_mins % 10;
    int minutes_tens = total_mins / 10;

    TM1637_display_digit(0x02, seconds_tens);
    TM1637_display_digit(0x03, seconds_units);

    TM1637_display_digit(0x00, minutes_tens);
    TM1637_display_digit(0x01, minutes_units);
}

void blink_digits() {
  TM1637_clear();
  _delay_ms(500);
  write_digits();
  _delay_ms(500);
}

void play_buzzer(int times, long micro_seconds) {
  for (int i = 0; i < times; i++) {
    setBit(BUZZER);
    _delay_ms(micro_seconds);
    clearBit(BUZZER);
    _delay_ms(micro_seconds);
  }
}

void change_state(State new_state) {
  current_state = new_state;

  switch (new_state) {
  case WORK:
    seconds = WORK_TIME;
    break;

  case REST:
    seconds = REST_TIME;
    break;

  case LONG_REST:
    seconds = LONG_REST_TIME;
    break;
  }
}
