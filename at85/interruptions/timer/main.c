#ifndef F_CPU
#define F_CPU 8000000L
#endif

#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint8_t i = 0;

static inline void initTimer1(void) {
  TCCR1 |= (1 << CTC1); // clear timer on compare match
  TCCR1 |= (1 << CS13) | (1 << CS12) | (1 << CS11) | (1 << CS10) ; // clock prescaler 16384
  OCR1C = 488; // compare match value
  TIMSK |= (1 << OCIE1A); // enable compare match interrupt
}

ISR (TIMER1_COMPA_vect) {
  if (i == 15) {
    i = 0;
  } else {
    i++;
  }

  PORTB = i;
}

int main(void) {

  // PB0 - PB3 as outputs
  DDRB = 0x0F;

  initTimer1();
  sei();

  while (1) {

  }


  return 0;
}

