#ifndef F_CPU
#define F_CPU 8000000L
#endif

#include <util/delay.h>

// Attiny85 I/O register begin at 0x0020 - 0x005F
// PORTB 0x18
#define PUERTOB *((volatile uint8_t*) 0x38)
// DDRB 0x17
#define SUPERDDRB *((volatile uint8_t*) 0x37)

int main(void) {

  SUPERDDRB = 0x01;

  while (1) {
    PUERTOB = 0x01;
    _delay_ms(1000);

    PUERTOB = 0x00;
    _delay_ms(1000);
  }

  return 1;
}
