#ifndef F_CPU
#define F_CPU 8000000L
#endif

#include <avr/io.h>
#include <util/delay.h>

int main(void) {

  // PB0 - PB3 as outputs
  DDRB = 0x0F;

  int shiftToLeft = 1;
  int value = 0x01;

  while (1) {
    PORTB = value;
    _delay_ms(1000);

    if (value == 0x01) {
      shiftToLeft = 1;
    }
    if (value == 0x08) {
      shiftToLeft = 0;
    }

    if (shiftToLeft) {
      value = value << 1;
    } else {
      value = value >> 1;
    }


  }

  return 0;
}
