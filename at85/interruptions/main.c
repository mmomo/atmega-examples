#ifndef F_CPU
#define F_CPU 8000000L
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define setBit(port, bit) port |= (1 << bit)
#define clearBit(port, bit) port &= ~(1 << bit)

ISR (INT0_vect) {
  // toggle PB1
  PORTB ^= (1 << PB1);
}

void external_interrupt() {
  sei();

  // enable the INT0 external interrupt
  GIMSK |= (1 << INT0);

  // configure as falling edge
  MCUCR |= (1 << ISC01);
}

int main(void) {

  external_interrupt();

  // 0000 0011 PB0 and PB1 as outputs
  DDRB = 0x03;

  while (1) {
    setBit(PORTB, PB0);
    _delay_ms(500);
    clearBit(PORTB, PB0);
    _delay_ms(500);
  }
}
